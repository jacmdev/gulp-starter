# About

Project starter using Gulp and its ecosystem.

- Sass
- Autoprefixer
- Babel
- Source Maps
- Minification
- BrowserSync
- EditorConfig
- Stylelint
- ESLint

# Installation

1. Clone repository
2. Install required packages

```
npm ci
```

# Use

## Watch

Tools will automatically watch for changes and rebuild project source files.

```
npm run watch
```

## Live server

Tools will automatically watch for changes, rebuild project source files and update page http://localhost:3000 in a web browser.

```
npm start
```

## Builds

Build project source files:

- For delevelopment
    - include source maps

```
npm run build
```

- For production

```
npm run build:prod
```

# Project structure

- `src/scripts/` - all JavaScript files from this directory will be concatenated into one main `.js` file
- `src/styles/index.scss` - entry point for styles
- all files and directories from `src/` (except `scripts/`, `styles/`) will be copied into `dist/`
