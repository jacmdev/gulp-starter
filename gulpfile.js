const production = process.env.NODE_ENV === 'production';
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const del = require('del');
const gulp = require('gulp');
const gulpif = require('gulp-if');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

const srcDir = 'src';
const srcScripts = `${srcDir}/scripts`;
const srcStyles = `${srcDir}/styles`;
const copyPaths = [`${srcDir}/**/*`, `!${srcScripts}`, `!${srcScripts}/**/*`, `!${srcStyles}`, `!${srcStyles}/**/*`];
const destDir = 'dist';
const scriptsOutput = 'scripts.min.js';
const stylesOutput = 'styles.min.css';

function clean() {
  return del([destDir]);
}

function copy() {
  return gulp.src(copyPaths, { since: gulp.lastRun(copy) }).pipe(gulp.dest(destDir));
}

function scripts() {
  return gulp
    .src(`${srcScripts}/**/*.js`)
    .pipe(gulpif(!production, sourcemaps.init()))
    .pipe(babel({ presets: ['@babel/env'] }))
    .pipe(uglify())
    .pipe(concat(scriptsOutput))
    .pipe(gulpif(!production, sourcemaps.write('./')))
    .pipe(gulp.dest(destDir));
}

function styles() {
  return gulp
    .src(`${srcStyles}/index.scss`)
    .pipe(gulpif(!production, sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(rename(stylesOutput))
    .pipe(gulpif(!production, sourcemaps.write('./')))
    .pipe(gulp.dest(destDir))
    .pipe(browserSync.stream());
}

function sync() {
  return browserSync.init({
    notify: false,
    open: false,
    server: { baseDir: destDir },
  });
}

function watch() {
  gulp.watch(`${srcScripts}/**/*.js`, scripts);
  gulp.watch(`${srcStyles}/**/*.scss`, styles);
  gulp.watch(copyPaths).on('change', copy);
  gulp.watch([`${destDir}/**/*`, `!${destDir}/${stylesOutput}`]).on('change', browserSync.reload);
}

const build = gulp.series(clean, copy, gulp.parallel(scripts, styles));
const serve = gulp.series(build, gulp.parallel(watch, sync));

exports.build = build;
exports.serve = serve;
exports.watch = watch;
